import { ConfigService } from './config-service/config-service';

export abstract class TaskProcessor {
  protected constructor(protected readonly _configService: ConfigService) {}

  process(params: unknown): Promise<unknown> {
    throw new Error('Not Implemented');
  }
}
