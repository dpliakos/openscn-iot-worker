export interface IConfig {
  redis: {
    host: string;
    port: number;
    username: string;
    password: string;
  };
  influx: {
    host: string;
    token: string;
    org: string;
    bucket: string;
  };
  app: {
    concurrency: number;
    jobsQueue: string;
    resultsQueue: string;
    version?: string;
  };
}

export class ConfigService {
  private readonly _config: IConfig;

  constructor() {
    this._config = {
      redis: {
        host: <string>process.env.REDIS_HOST ?? '127.0.0.1',
        port: process.env.REDIS_PORT ? Number(process.env.REDIS_PORT) : 6379,
        username: <string>process.env.REDIS_USERNAME ?? 'default',
        password: <string>process.env.REDIS_PASSWORD,
      },
      influx: {
        host: <string>process.env.INFLUX_URL,
        bucket: <string>process.env.INFLUX_BUCKET,
        org: <string>process.env.INFLUX_ORG,
        token: <string>process.env.INFLUX_TOKEN,
      },
      app: {
        concurrency: process.env.APP_CONCURENT_JOBS
          ? Number(process.env.APP_CONCURENT_JOBS)
          : 4,
        jobsQueue: <string>process.env.APP_JOBS_QUEUE ?? 'iot-worker-jobs',
        resultsQueue:
          <string>process.env.APP_RESULTS_QUEUE ?? 'iot-worker-results',
        version: process.env.APP_VERSION ? process.env.APP_VERSION : process.env.npm_package_version,
      },
    };
  }

  get config(): IConfig {
    return this._config;
  }
}
