import { Job, Queue, Worker } from 'bullmq';
import * as dotenv from 'dotenv';
import { TaskProcessor } from './processor';
import { ThresholdCheckProcessor } from './threshold-check/threshold-check';
import { ConfigService } from './config-service/config-service';
import { Logger } from './logger/logger';
import { InfluxQueryService } from './influx-query-service/influx-query-service';
import { SupportedJobs } from './constants';
import { ProcessResult } from './types';
dotenv.config();

const configService = new ConfigService();
const config = configService.config;
const workerName = `openscn-iot-worker@${config.app.version}`;
const logger = new Logger(workerName);
const queryService = new InfluxQueryService(config.influx, logger);

const worker = new Worker(
  config.app.jobsQueue,
  async (job: Job) => {
    const { name, payload } = job.data;
    let processor: TaskProcessor | undefined = undefined;

    switch (name) {
      case SupportedJobs.ThresholdCheck:
        processor = new ThresholdCheckProcessor(configService, queryService, logger);
        break;
      default:
        throw new Error(`Could not initialize processor for task ${name}`);
    }

    return await processor.process(payload);
  },
  {
    concurrency: config.app.concurrency,
    connection: {
      host: config.redis.host,
      port: config.redis.port,
      username: config.redis.username,
      password: config.redis.password,
    },
  }
);

const resultsQueue = new Queue(config.app.resultsQueue, {
  connection: {
    host: config.redis.host,
    port: config.redis.port,
    username: config.redis.username,
    password: config.redis.password,
  },
});

worker.on('ready', () => {
  logger.log(`Ready. Listening on ${config.app.jobsQueue}`);
});

worker.on('active', (job) => {
  logger.log(`Active -`, job.name, ' - ', job.id);
});

worker.on('completed', (job: Job<ProcessResult>) => {
  logger.log(`Completed job ${job.name} - ${job.id}`);

  if (job?.returnvalue?.shouldReport) {
    resultsQueue
      .add(config.app.resultsQueue, {
        status: 'completed',
        payload: job.returnvalue.data,
        version: config.app.version,
        parentJob: job.parentKey,
      })
      .then((resultsJob) => {
        logger.log(
          `Pushed results for ${job.name}[${job.id}] as results job ${resultsJob.id}`
        );
      })
      .catch((err) => {
        logger.error(err);
      });
  }
});

worker.on('error', (err) => {
  logger.log(`Error`, err);
});

worker.on('failed', (job, error, prev) => {
  logger.log(`Error while processing job ${job?.id} - `, error);
  resultsQueue
    .add(config.app.resultsQueue, {
      status: 'failed',
      version: config.app.version,
      error: error,
      parentJob: job?.parentKey,
      payload: {
        task: job?.data?.payload?.task,
      },
    })
    .then((resultsJob) => {
      logger.log(
        `Pushed results for ${job?.id} as results job ${resultsJob.id}`
      );
    })
    .catch((err) => {
      logger.error(err);
    });
});

worker.on('closed', () => {
  logger.log(`Closed`);
});

worker.on('ioredis:close', () => {
  logger.log(`Redis connection closed`);
});

worker.on('closing', () => {
  logger.log(`Closing`);
});

process.on('uncaughtException', async (err) => {
  logger.error('UncaughtException');
  await worker.close();
  logger.error(err);
  process.exit(1);
});

process.on('unhandledRejection', async (err) => {
  logger.error('UnhandledRejection');
  await worker.close();
  logger.error(err);
  process.exit(2);
});
