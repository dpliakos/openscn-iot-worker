import { TaskProcessor } from '../processor';
import { ConfigService } from '../config-service/config-service';
import { InfluxQueryService } from '../influx-query-service/influx-query-service';
import { Operator } from '../influx-query-service/query-service.types';
import { ProcessResult } from '../types';
import { Logger } from "../logger/logger";

export interface ThresholdCheckProcessorParams {
  measurement: string;
  start: string;
  stop: string;
  warningThreshold: number;
  criticalThreshold: number;
  operator: Operator;
  task: string;
}

interface StatusResponse {
  task: string;
  status: 'normal' | 'critical' | 'warning' | 'empty';
  value: number;
  values: number[],
}

interface StatusInstance {
  status: StatusResponse['status'];
  timestamp: string;
}

interface ResolveStatusOptions {
  number: number,
  operator: Operator,
  warningThreshold: number;
  criticalThreshold: number;
}

export class ThresholdCheckProcessor extends TaskProcessor {
  constructor(
    protected readonly _configService: ConfigService,
    private readonly _influxService: InfluxQueryService,
    private readonly _logger: Logger,
  ) {
    super(_configService);
  }

  async process(params: ThresholdCheckProcessorParams): Promise<ProcessResult> {
    const currentStatuses = await this.queryLastStatus(params);
    if (currentStatuses.length > 0) {
      params.start = currentStatuses[0].timestamp;
    }
    const data = await this.queryData(params);

    if (data.status !== 'empty') {
      await this.storeStatus(params, data);
      const statuses = await this.queryLastStatus(params);
      const shouldAlert = this.shouldAlert(statuses);
      this._logger.debug(`Status: `, data.status, ', prev: ', statuses[1], 'alert: ', shouldAlert, 'for value', data.value, 'with params: ', params, 'in set', data.values);
      return {
        status: 'success',
        shouldReport: shouldAlert.value,
        data: {
          prevStatus: statuses[1]?.status,
          status: statuses[0].status,
          task: params.task,
          measurement: params.measurement,
          start: params.start,
          stop: params.stop,
          value: data.value,
        },
      };
    } else {
      this._logger.debug('Skipping check for empty set');
      return {
        status: 'success',
        shouldReport: false,
      };
    }
  }

  async queryData(
    params: ThresholdCheckProcessorParams
  ): Promise<StatusResponse> {
    return new Promise((resolve, reject) => {
      const { start, stop, measurement, warningThreshold, criticalThreshold, operator } = params;
      const influxConfig = this._configService.config.influx;

      const query = `
      from(bucket: "${influxConfig.bucket}")
        |> range(start: ${start}, stop: ${stop ?? 'now()'})
        |> filter(fn: (r) => r["_measurement"] == "${measurement}")
    `;

      this._logger.debug(query);
      let hasWarning = false;
      let hasValues = false;
      let value: number;
      const values: number[] = [];

      this._influxService.query(query, {
        next: (row, tableMeta) => {
          hasValues = true;
          const tableObject = tableMeta.toObject(row);
          value = Number(tableObject._value);
          values.push(value);

          const status = this.resolveStatus({
            number: value,
            criticalThreshold: criticalThreshold,
            warningThreshold: warningThreshold,
            operator: operator,
          });

          if (status === 'critical') {
            return resolve({
              status: 'critical',
              task: params.task,
              value: value,
              values: values,
            });
          } else if (status === 'warning') {
            hasWarning = true;
          }
        },
        error: (error) => {
          return reject(error);
        },
        complete: () => {
          if (!hasValues) {
            return resolve({
              status: 'empty',
              task: params.task,
              value: value,
              values: values,
            });
          } else if (hasWarning) {
            return resolve({
              status: 'warning',
              task: params.task,
              value: value,
              values: values,
            });
          } else {
            return resolve({
              status: 'normal',
              task: params.task,
              value: value,
              values: values,
            });
          }
        },
      });
    });
  }

  async storeStatus(
    params: ThresholdCheckProcessorParams,
    data: StatusResponse
  ) {
    await this._influxService.store({
      measurement: params.task,
      targetId: params.task,
      type: '',
      fields: [
        {
          value: data.status,
        },
      ],
      timestamp: Date.now(),
      tags: [
        {
          source: `openscn-iot-worker@${this._configService.config.app.version}`,
        },
      ],
    });
  }

  async queryLastStatus(
    params: ThresholdCheckProcessorParams
  ): Promise<StatusInstance[]> {
    return new Promise((resolve, reject) => {
      const { task } = params;
      const influxConfig = this._configService.config.influx;

      const query = `
      from(bucket: "${influxConfig.bucket}")
        |> range(start: 0, stop: now())
        |> filter(fn: (r) => r["_measurement"] == "${task}")
        |> filter(fn: (r) => r["_field"] == "value")
        |> top(n: 2, columns: ["_time"])
    `;

      const values: StatusInstance[] = [];

      this._influxService.query(query, {
        next: (row, tableMeta) => {
          const tableObject = tableMeta.toObject(row);
          values.push({
            status: tableObject._value,
            timestamp: tableObject._time,
          });
        },
        error: (error) => {
          return reject(error);
        },
        complete: () => {
          return resolve(values);
        },
      });
    });
  }

  shouldAlert(statuses: StatusInstance[]) {
    const [current, prev] = statuses;

    const isNew = !prev && (current.status === 'critical' || current.status === 'warning');
    const hasChanged = prev?.status !== current.status;

    const result = {
      reason: isNew ? 'new' : 'change',
      value: isNew || hasChanged,
    }

    return result;
  }


  /**
   *
   * Given a set of parameters for value, warning and critical thresholds, it
   * resolves the nominal status of the value
   *
   */
  resolveStatus(options: ResolveStatusOptions): StatusInstance['status'] {
    const { operator, number, warningThreshold, criticalThreshold} = options;
    const isCritical = this._influxService.compare(operator, number, criticalThreshold);
    const isWarning = warningThreshold !== null && !isNaN(warningThreshold) && this._influxService.compare(operator, number, warningThreshold);

    if (isCritical) {
      return 'critical';
    } else if (isWarning) {
      return 'warning';
    } else {
      return 'normal';
    }
  }
}
