import { InfluxQueryService } from "./influx-query-service";
import { Logger } from "../logger/logger";
import { describe } from "node:test";

describe(InfluxQueryService.name, () => {
  let service: InfluxQueryService;

  beforeEach(() => {
    service = new InfluxQueryService({
      host: '',
      token: '',
      bucket: '',
      org: '',
    },
      new Logger(''), true);
  });

  it('should create', () => {
    expect(service).toBeDefined();
  });

  describe('The comapre method', () => {
    it('should compare for eq', () => {
      expect(service.compare("eq", 10, 10)).toBeTruthy();
      expect(service.compare("eq", 11, 11)).toBeTruthy();
      expect(service.compare("eq", 0, 0)).toBeTruthy();
      expect(service.compare("eq", 0, -0)).toBeTruthy();
      expect(service.compare("eq", -100, -100)).toBeTruthy();
      expect(service.compare("eq", Number.MAX_VALUE, Number.MAX_VALUE)).toBeTruthy();
      expect(service.compare("eq", Number.MIN_VALUE, Number.MIN_VALUE)).toBeTruthy();

      expect(service.compare("eq", 10, -100)).toBeFalsy();
    });

    it('should compare for gt', () => {
      expect(service.compare("gt", 13, 10)).toBeTruthy();
      expect(service.compare("gt", 13, -1)).toBeTruthy();
      expect(service.compare("gt", 0, -1)).toBeTruthy();
      expect(service.compare("gt", Number.MAX_VALUE, 1)).toBeTruthy();

      expect(service.compare("gt", 0, 1)).toBeFalsy();
      expect(service.compare("gt", 1, 1)).toBeFalsy();
    });

    it('should compare for ge', () => {
      expect(service.compare("ge", 13, 10)).toBeTruthy();
      expect(service.compare("ge", 10, 10)).toBeTruthy();
      expect(service.compare("ge", 13, -1)).toBeTruthy();
      expect(service.compare("ge", 0, -1)).toBeTruthy();
      expect(service.compare("ge", 0, 0)).toBeTruthy();
      expect(service.compare("ge", Number.MAX_VALUE, 1)).toBeTruthy();

      expect(service.compare("ge", 0, 1)).toBeFalsy();
    });

    it('should compare for lt', () => {
      expect(service.compare("lt", 10, 13)).toBeTruthy();
      expect(service.compare("lt", -1, 13)).toBeTruthy();
      expect(service.compare("lt", -1, -0)).toBeTruthy();
      expect(service.compare("lt", 1, Number.MAX_VALUE)).toBeTruthy();

      expect(service.compare("lt", 1, 0)).toBeFalsy();
      expect(service.compare("lt", 1, 1)).toBeFalsy();
    });

    it('should compare for le', () => {
      expect(service.compare("le", 10, 13)).toBeTruthy();
      expect(service.compare("le", 10, 10)).toBeTruthy();
      expect(service.compare("le", -1, 13 )).toBeTruthy();
      expect(service.compare("le", -1, 0)).toBeTruthy();
      expect(service.compare("le", 0, 0)).toBeTruthy();
      expect(service.compare("le", 1, Number.MAX_VALUE)).toBeTruthy();

      expect(service.compare("le", 1, 0)).toBeFalsy();
    });
  });
});
