import { Operator } from './query-service.types';
import {
  FluxResultObserver,
  InfluxDB,
  ParameterizedQuery,
  Point,
} from '@influxdata/influxdb-client';
import { Logger } from '../logger/logger';

interface InluxOptions {
  host: string;
  token: string;
  bucket: string;
  org: string;
}

/**
 *
 * @Interface InfluxEntry
 *
 * @property measurement The unique id of the measurement
 * @property targetId The identifier of the entity that uses that measurement
 * @property type The type of the entity that uses that measurement
 *
 */
export interface InfluxEntry {
  measurement: string;
  targetId: string;
  type: string;
  tags: Record<string, string>[];
  fields: Record<string, string | number | boolean>[];
  timestamp: number;
}

export class InfluxQueryService {
  private readonly client;

  constructor(
    private readonly options: InluxOptions,
    private readonly _logger: Logger,
    skipClient = false,
  ) {
    if (!skipClient) {
      this.client = new InfluxDB({
        url: options.host,
        token: options.token,
      });
    }
  }

  query(
    query: string | ParameterizedQuery,
    consumer: FluxResultObserver<string[]>
  ) {
    const api = this.client?.getQueryApi(this.options.org);
    return api?.queryRows(query, consumer);
  }

  async store(measurement: InfluxEntry) {
    const writeApi = this.client?.getWriteApi(
      this.options.org,
      this.options.bucket
    );

    const p = new Point(measurement.measurement);
    p.tag('target_id', measurement.targetId);
    p.tag('type', measurement.type);
    p.stringField('value', measurement.fields[0]['value']);
    await writeApi?.writePoint(p);

    try {
      await writeApi?.flush();
      await writeApi?.close();
      this._logger.debug(
        `Collected measurement point for ${measurement.measurement}`
      );
    } catch (err) {
      this._logger.error((err as Error)?.stack ?? err);
      throw err;
    }
  }

  compare(operator: Operator, left: number, right: number) {
    switch (operator) {
      case 'eq':
        return left == right;
      case 'gt':
        return left > right;
      case 'ge':
        return left >= right;
      case 'lt':
        return left < right;
      case 'le':
        return left <= right;
      default:
        return false;
    }
  }
}
