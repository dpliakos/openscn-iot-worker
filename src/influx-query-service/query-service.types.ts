export type Operator = 'eq' | 'gt' | 'ge' | 'lt' | 'le';
